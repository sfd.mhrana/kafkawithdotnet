﻿using Confluent.Kafka;
using System.ComponentModel;
using System.Diagnostics;

namespace KafkaTesting.Service
{
    public class ConsumerService
    {
        private readonly string topic = "tutorialspedia";
        private readonly string bootstrapServers = "localhost:9092";
        private BackgroundWorker _worker;
        public ConsumerService()
        {
            _worker = new BackgroundWorker();
            _worker.DoWork += DoBackgroundWork;
            _worker.RunWorkerAsync();
        }

        public void DoBackgroundWork(object sender, DoWorkEventArgs e)
        {
            var config = new ConsumerConfig
            {
                BootstrapServers = bootstrapServers,
                GroupId= "tutorialspedia_group",
                AutoOffsetReset = AutoOffsetReset.Earliest
            };

            try
            {
                using (var consumerBuilder = new ConsumerBuilder<Ignore, string>(config).Build())
                {
                    consumerBuilder.Subscribe(topic);
                    var cancelToken = new CancellationTokenSource();

                    try
                    {
                        while (true)
                        {
                            var consumer = consumerBuilder.Consume(cancelToken.Token);
                            if(consumer.Message != null && consumer.Message.Value != null)
                            {
                                Debug.WriteLine("Processing Order Id"+ consumer.Message.Value);
                            }
                        }
                    }
                    catch (OperationCanceledException)
                    {
                        consumerBuilder.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
