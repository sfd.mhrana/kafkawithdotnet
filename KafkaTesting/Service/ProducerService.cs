﻿using Confluent.Kafka;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using static Confluent.Kafka.ConfigPropertyNames;

namespace KafkaTesting.Service
{
    public class ProducerService
    {

        public async Task<string> ProduceMessage(string message)
        {
            var config = new ProducerConfig
            {
                BootstrapServers = "localhost:9092",
                ClientId = Dns.GetHostName()
            };

            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {
                var result = await producer.ProduceAsync("tutorialspedia", new Message<Null, string> { Value = message });
                return result.Value;
            }
        }
    }

}

