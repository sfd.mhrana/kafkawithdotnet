﻿using Confluent.Kafka;
using KafkaTesting.Service;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private readonly ProducerService _producerS;
       
        public MessagesController(ProducerService producerS)
        {
            _producerS = producerS;
        }

        [HttpPost]
        public async Task<IActionResult> SendMessage(string message)
        {
            await _producerS.ProduceMessage(message);
            return Ok();
        }
    }
}
